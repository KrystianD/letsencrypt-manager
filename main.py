#!/usr/bin/python3
import traceback
from typing import List, Any

import yaml, subprocess, os, glob

# from pydantic.dataclasses import dataclass

# script_dir = os.path.dirname(os.path.realpath(__file__))
# os.chdir(script_dir)


def extend_certbot_command_with_cloudflare(args: List[str], credentials_path: str):
    args += ["--dns-cloudflare",
             "--dns-cloudflare-credentials", credentials_path,
             "--dns-cloudflare-propagation-seconds", str(15)]


def extend_certbot_command_with_google(args: List[str], credentials_path: str):
    args += ["--dns-google",
             "--dns-google-credentials", credentials_path,
             "--dns-google-propagation-seconds", str(90)]


def perform_command_on_group(command: str, dns_config: Any, group, quiet: bool):
    sites = group["group"]

    if command in ("renew", "force-renew"):

        args = [
            "certbot", "certonly", "--noninteractive",
            "--expand", "--text", "--keep",
            "--no-self-upgrade", "--no-bootstrap",
        ]

        if quiet:
            args += ["--quiet"]

        if "cloudflare" in dns_config:
            extend_certbot_command_with_cloudflare(args, dns_config["cloudflare"])
        if "google" in dns_config:
            extend_certbot_command_with_google(args, dns_config["google"])

        if command == "force-renew":
            args += ["--force-renewal"]

        cert_name = sites[0]
        for s in sites:
            args += ["-d", s]

        if not quiet:
            print(" ".join(args))
        subprocess.check_call(args)

        for s in sites:
            certdir = sorted(glob.glob(f"/etc/letsencrypt/live/{cert_name}*"))[-1]

            p = f"/etc/nginx/gen/fullchain-{s}.pem"
            if os.path.lexists(p):
                os.unlink(p)
            os.symlink(os.path.join(certdir, "fullchain.pem"), p)

            p = f"/etc/nginx/gen/privkey-{s}.pem"
            if os.path.lexists(p):
                os.unlink(p)
            os.symlink(os.path.join(certdir, "privkey.pem"), p)


def main():
    import argparse

    argparser = argparse.ArgumentParser()
    argparser.add_argument('-c', '--config', type=str, metavar="PATH", required=True)
    argparser.add_argument('-q', '--quiet', action="store_true")
    argparser.add_argument('command', choices=["renew", "force-renew"])
    args = argparser.parse_args()

    with open(args.config) as f:
        data = yaml.load(f, Loader=yaml.SafeLoader)

    all_ok = True

    for group in data["certs"]:
        try:
            perform_command_on_group(args.command, data["dns"], group, args.quiet)
        except KeyboardInterrupt:
            exit(0)
        except:
            traceback.print_exc()
            all_ok = False
            pass

    subprocess.check_call(["/etc/init.d/nginx", "reload"], stdout=subprocess.DEVNULL)

    if not all_ok:
        exit(1)


if __name__ == "__main__":
    main()
